geo      = 'SE'                        # geo indicator (location)

measure  = 'cases'                     # file column to use as measure
smeasure = 'Week window'               # smoothed measures
rmeasure = 'rcases'                    # remaining measures after iteration
pmeasure = 'Model'                     # projected measures summed
wmeasure = 'Wave '                     # wave name prefix, zero-leading number is added
wavenum  = 2                           # wave numbering width

sdays    = 7                           # number of days for smoothing window
firstwav = 1                           # initial wave number
popcases = 1e6                         # relative minimum from population size (one in n cases)
mincases = 2                           # absolute minimum number of cases to consider

linmax   = -1/2                        # upper bound of linear derivation
linmin   = -9/2                        # lower bound of linear derivation
betamax  = 33                          # maximum accepted beta estimate
datamin  = 3                           # minimum number of data points in spline
projmin  = 1                           # minimum cases for projected start and floor
gradmin  = 1e-6                        # minimum gradient difference for knot

plotmin  = '20200101'                  # start date of output plots
plotmax  = '20210101'                  # stop date of output plots

earth = {'minspan'     : 1,
         'penalty'     : 0,
         'endspan'     : 0,
         'thresh'      : 1e-9,
         'check_every' : 1}