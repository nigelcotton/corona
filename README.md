# Exploring, modeling & visualizing reported case data

I'm not an epidemiologist or a virologist and just exploring data, so I will not draw conclusions on that level nor base behavior or policy upon these explorations and neither should you. Even within a country the data is heavily biased, not in the least because of testing and measurement policies changing over time!

If you need a runtime environment use this access link:

https://cloud.ibm.com/

Set up your environment this way:

https://github.com/IBM/skillsnetwork/wiki/Watson-Studio-Setup

Try this example of a pipeline implementation:

https://developer.ibm.com/technologies/data-science/blogs/open-source-jupyter-notebooks-analyze-covid-19-data/
